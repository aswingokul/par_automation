/**
 * 
 */
package testcases;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import apis.GenerateToken;
import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class Statistics {
	
//	@Test
	public void getStatistics() {
		HttpClient client = HttpClientBuilder.create().build();
		ExtentTest jt = ExtentTestManager.startTest("Get statistics", "Gets the statistics");
		jt.log(LogStatus.INFO, "Starting statistics get....");
		String server = "http://amazon-ads-qa-1968130785.ap-southeast-1.elb.amazonaws.com/api/v4";
		String url = server + "/statistics/get?env=qa&access_token="+GenerateToken.generateToken()+"&job_id=J1053";
		jt.log(LogStatus.INFO, "url: " + url);
		HttpGet get = new HttpGet(url);
		
		try {
			HttpResponse hr = client.execute(get);
			String res = EntityUtils.toString(hr.getEntity());
			JsonObject json = new JsonParser().parse(res).getAsJsonObject();
			System.out.println("json: " + json.toString());
			jt.log(LogStatus.INFO, "json: " + json.toString());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
