/**
 * 
 */
package testcases;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import apis.GenerateToken;
import apis.RecoSet;
import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class Recommendations extends BaseTest {

	String recoSetID = null;
	String adID = "[\"adavick7\"]";

	@Test(priority = 0)
	@Parameters({ "server" })
	public void createRecommendation(String server) {
		HttpClient client = HttpClientBuilder.create().build();
		ExtentTest crReco = ExtentTestManager.startTest("Create Recommendation", "Creates a new recommendation");
		crReco.log(LogStatus.INFO, "Creating a new recommendation set...");

		try {

			// creating a new reco set id
			HttpPost recoSet = RecoSet.createRSPostReq(server);
			HttpResponse recoSetResp = client.execute(recoSet);
			String response = EntityUtils.toString(recoSetResp.getEntity());

			JsonParser parser = new JsonParser();
			JsonObject json = parser.parse(response).getAsJsonObject();

			JsonObject data = json.get("data").getAsJsonObject();
			this.recoSetID = data.get("recommendation_set_id").getAsString();

			HttpPost post = apis.Recos.createRecommendation(server, this.recoSetID);
			HttpResponse res = client.execute(post);
			String recos = EntityUtils.toString(res.getEntity());
			System.out.println("Response: " + recos);
			crReco.log(LogStatus.INFO, "Response: " + recos);

			JsonObject recoJson = parser.parse(recos).getAsJsonObject();

			validateRecosSmall(recoJson);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 1)
	@Parameters({ "server" })
	public void getRecommendationWithRecoSetID(String server) {
		try {

			HttpClient client = HttpClientBuilder.create().build();
			ExtentTest getReco = ExtentTestManager.startTest("Get Recommendation using reco set id",
					"Gets Recommendation using reco set id");
			getReco.log(LogStatus.INFO, "Getting the recommendation with id " + this.recoSetID);

			String url = server + "/recommendation/get?env=qa&recommendation_set_id=" + this.recoSetID
					+ "&access_token=" + GenerateToken.generateToken();
			System.out.println("url: " + url);

			HttpGet get = new HttpGet(url);
			HttpResponse gets = client.execute(get);
			String response = EntityUtils.toString(gets.getEntity());
			System.out.println("response: " + response);
			getReco.log(LogStatus.INFO, "response: " + response);

			JsonObject json = new JsonParser().parse(response).getAsJsonObject();
			validateRecosBig(json);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

//	@Test(priority = 2)
	@Parameters({ "server" })
	public void getRecommendationWithAdID(String server) {
		try {

			HttpClient client = HttpClientBuilder.create().build();
			ExtentTest getReco = ExtentTestManager.startTest("Get Recommendation using ad id",
					"Gets Recommendation using ad id");
			getReco.log(LogStatus.INFO, "Getting the recommendation ad id " + this.adID);

			String url = server + "/recommendation/get?env=qa&recommendation_set_id=" + this.recoSetID
					+ "&access_token=" + GenerateToken.generateToken() + "&ad_ids=" + this.adID;
			System.out.println("url: " + url);
			URIBuilder builder = new URIBuilder(url);
			builder.setParameter("ad_ids", URLEncoder.encode(this.adID, "UTF-8"));
			HttpGet get = new HttpGet(builder.build());
			HttpResponse gets = client.execute(get);
			String response = EntityUtils.toString(gets.getEntity());
			System.out.println("response: " + response);
			getReco.log(LogStatus.INFO, "response: " + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(priority = 3)
	@Parameters({ "server" })
	public void deleteReco(String server) {
		ExtentTest delReco = ExtentTestManager.startTest("Delete the recommendation", "Delete the recommendation");
		HttpClient client = HttpClientBuilder.create().build();
		delReco.log(LogStatus.INFO, "Deleting a reco....");

		String url = server + "/recommendation/delete?env=qa&access_token=" + GenerateToken.generateToken()
				+ "&recommendation_set_id=" + this.recoSetID;
		System.out.println("ur: " + url);

		HttpPut put = new HttpPut(url);
		try {
			HttpResponse hr = client.execute(put);
			String res = EntityUtils.toString(hr.getEntity());
			System.out.println("res: " + res);
			delReco.log(LogStatus.INFO, "res: " + res);

			JsonObject json = new JsonParser().parse(res).getAsJsonObject();
			validateDeleteReco(json);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void validateRecosSmall(JsonObject json) {
		if (sa != null) {
			if (json.has("data")) {
				JsonObject data = json.get("data").getAsJsonObject();
				if (data.has("recommendation_set_id")) {
					String rsid = data.get("recommendation_set_id").getAsString();
					sa.assertTrue(rsid != null && !rsid.isEmpty());
					this.recoSetID = rsid;
				} else {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "recommendation_set_id is missing");
				}

				if (data.has("external_recommendation_set_id")) {
					String rsid = data.get("external_recommendation_set_id").getAsString();
					sa.assertTrue(rsid != null && !rsid.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "external_recommendation_set_id is missing");
				}

				if (data.has("job_id")) {
					if (data.get("job_id").isJsonNull()) {
						ExtentTestManager.getTest().log(LogStatus.WARNING, "job_id is null");
					} else {
						String job_id = data.get("job_id").getAsString();
						sa.assertTrue(job_id != null && !job_id.isEmpty());
					}

				} else {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "job_id is missing");
				}

				if (data.has("status")) {
					String status = data.get("status").getAsString();
					sa.assertTrue(status != null && !status.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "data-status is missing");
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "data is missing");
			}

			if (json.has("status")) {
				String status = json.get("status").getAsString();
				sa.assertTrue(status != null && !status.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "status is missing");
			}

			if (json.has("totalResults")) {
				String totalResults = json.get("totalResults").getAsString();
				sa.assertTrue(totalResults != null && !totalResults.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "totalResults is missing");
			}

			if (json.has("code")) {
				String code = json.get("code").getAsString();
				sa.assertTrue(code != null && !code.isEmpty() && code.equals("200"));
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "code is missing");
			}

			if (json.has("message")) {
				String message = json.get("message").getAsString();
				sa.assertTrue(message != null && !message.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "message is missing");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.ERROR, "SoftAssert instance is null");
		}
	}

	void validateRecosBig(JsonObject json) {

		if (sa != null) {
			if (json.has("data")) {
				JsonObject data = json.get("data").getAsJsonObject();
				Set<Entry<String, JsonElement>> entrySet = data.entrySet();
				for (Map.Entry<String, JsonElement> entry : entrySet) {
					JsonArray valArr = entry.getValue().getAsJsonArray();
					int arrSize = valArr.size();
					for (int i = 0; i < arrSize; i++) {
						validateadIdObjs(valArr.get(i).getAsJsonObject());
					}
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "data is missing");
			}

			if (json.has("status")) {
				String status = json.get("status").getAsString();
				sa.assertTrue(status != null && !status.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "status is missing");
			}

			if (json.has("totalResults")) {
				String totalResults = json.get("totalResults").getAsString();
				sa.assertTrue(totalResults != null && !totalResults.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "totalResults is missing");
			}

			if (json.has("code")) {
				String code = json.get("code").getAsString();
				sa.assertTrue(code != null && !code.isEmpty() && code.equals("200"));
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "code is missing");
			}

			if (json.has("message")) {
				String message = json.get("message").getAsString();
				sa.assertTrue(message != null && !message.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "message is missing");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.ERROR, "SoftAssert instance is null");
		}

	}

	void validateadIdObjs(JsonObject json) {
		if (json.has("creative_id")) {
			String creative_id = json.get("creative_id").getAsString();
			sa.assertTrue(creative_id != null && !creative_id.isEmpty());
		}

		if (json.has("recommendation_set_id")) {
			String recommendation_set_id = json.get("recommendation_set_id").getAsString();
			sa.assertTrue(recommendation_set_id != null && !recommendation_set_id.isEmpty());
		}

		if (json.has("external_recommendation_set_id")) {
			if (json.get("external_recommendation_set_id").isJsonNull()) {
				ExtentTestManager.getTest().log(LogStatus.WARNING, "external_recommendation_set_id is null");
			} else {
				String external_recommendation_set_id = json.get("external_recommendation_set_id").getAsString();
				sa.assertTrue(external_recommendation_set_id != null && !external_recommendation_set_id.isEmpty());
			}

		}

		if (json.has("start_date")) {
			if (json.get("start_date").isJsonNull()) {
				ExtentTestManager.getTest().log(LogStatus.WARNING, "start_date is null");
			} else {
				String start_date = json.get("start_date").getAsString();
				sa.assertTrue(start_date != null && !start_date.isEmpty());
			}

		}

		if (json.has("end_date")) {
			if (json.get("end_date").isJsonNull()) {
				ExtentTestManager.getTest().log(LogStatus.WARNING, "end_date is null");
			} else {
				String end_date = json.get("end_date").getAsString();
				sa.assertTrue(end_date != null && !end_date.isEmpty());
			}

		}

		if (json.has("bid_multiplier")) {
			if (json.get("bid_multiplier").isJsonNull()) {
				ExtentTestManager.getTest().log(LogStatus.WARNING, "bid_multiplier is null");
			} else {
				String bid_multiplier = json.get("bid_multiplier").getAsString();
				sa.assertTrue(bid_multiplier != null && !bid_multiplier.isEmpty());
			}

		}

		if (json.has("advertiser")) {
			if (json.get("advertiser").isJsonNull()) {
				ExtentTestManager.getTest().log(LogStatus.WARNING, "advertiser is null");
			} else {
				String advertiser = json.get("advertiser").getAsString();
				sa.assertTrue(advertiser != null && !advertiser.isEmpty());
			}

		}

	}

	void validateDeleteReco(JsonObject json) {
		if (sa != null) {
			if (json.has("data")) {
				JsonObject data = json.get("data").getAsJsonObject();
				if (data.has("created_at")) {
					String created_at = data.get("created_at").getAsString();
					sa.assertTrue(created_at != null && !created_at.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "created_at is missing");
				}

				if (data.has("type")) {
					String type = data.get("type").getAsString();
					sa.assertTrue(type != null && !type.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "type is missing");
				}

				if (data.has("job_id")) {
					if (data.get("job_id").isJsonNull()) {
						ExtentTestManager.getTest().log(LogStatus.WARNING, "job_id is null");
					} else {
						String job_id = data.get("job_id").getAsString();
						sa.assertTrue(job_id != null && !job_id.isEmpty());
					}

				} else {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "job_id is missing");
				}

				if (data.has("status")) {
					String status = data.get("status").getAsString();
					sa.assertTrue(status != null && !status.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "data-status is missing");
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "data is missing");
			}

			if (json.has("status")) {
				String status = json.get("status").getAsString();
				sa.assertTrue(status != null && !status.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "status is missing");
			}

			if (json.has("totalResults")) {
				String totalResults = json.get("totalResults").getAsString();
				sa.assertTrue(totalResults != null && !totalResults.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "totalResults is missing");
			}

			if (json.has("code")) {
				String code = json.get("code").getAsString();
				sa.assertTrue(code != null && !code.isEmpty() && code.equals("200"));
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "code is missing");
			}

			if (json.has("message")) {
				String message = json.get("message").getAsString();
				sa.assertTrue(message != null && !message.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "message is missing");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.ERROR, "SoftAssert instance is null");
		}
	}

}
