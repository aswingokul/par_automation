/**
 * 
 */
package testcases;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import apis.GenerateToken;
import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class Job extends BaseTest {

	@Test
	public void getJob() {
		HttpClient client = HttpClientBuilder.create().build();
		ExtentTest jt = ExtentTestManager.startTest("Get Job", "Gets a job");
		jt.log(LogStatus.INFO, "Starting Job get....");
		String server = "http://amazon-ads-qa-1968130785.ap-southeast-1.elb.amazonaws.com/api/v4";
		String url = server + "/job/get?env=qa&access_token=" + GenerateToken.generateToken() + "&job_id=J1053";
		jt.log(LogStatus.INFO, "url: " + url);
		HttpGet get = new HttpGet(url);

		try {
			HttpResponse hr = client.execute(get);
			String res = EntityUtils.toString(hr.getEntity());
			JsonObject json = new JsonParser().parse(res).getAsJsonObject();
			System.out.println("job json: " + json.toString());
			jt.log(LogStatus.INFO, "json: " + json.toString());
			validateJobJson(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void validateJobJson(JsonObject json) {
		if (sa != null) {
			if (json.has("data")) {

				JsonObject data = json.get("data").getAsJsonObject();

				if (data.has("job_id")) {
					sa.assertTrue(!data.get("job_id").isJsonNull());
					sa.assertTrue(
							data.get("job_id").getAsString() != null || !data.get("job_id").getAsString().isEmpty());
				} else {
					sa.fail("job_id is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "job_id is missing..");
				}

				if (data.has("recommendation_set_id")) {
					if (!data.get("recommendation_set_id").isJsonNull()) {
						String recommendation_set_id = data.get("recommendation_set_id").getAsString();
						sa.assertTrue(!(recommendation_set_id != null) && !recommendation_set_id.isEmpty(),
								"recommendation_set_id is null or empty");
					} else {
						ExtentTestManager.getTest().log(LogStatus.WARNING, "recommendation_set_id is JsonNull");
					}

				} else {
					sa.fail("recommendation_set_id is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "recommendation_set_id is missing..");
				}

				if (data.has("external_recommendation_set_id")) {
					if (!data.get("external_recommendation_set_id").isJsonNull()) {
						String external_recommendation_set_id = data.get("external_recommendation_set_id")
								.getAsString();
						sa.assertTrue(
								!(external_recommendation_set_id != null) && !external_recommendation_set_id.isEmpty(),
								"external_recommendation_set_id is null or empty");
					} else {
						ExtentTestManager.getTest().log(LogStatus.WARNING,
								"external_recommendation_set_id is JsonNull");
					}
				} else {
					sa.fail("external_recommendation_set_id is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "external_recommendation_set_id is missing..");
				}

				if (data.has("status")) {
					sa.assertTrue(!data.get("status").isJsonNull() && (data.get("status").getAsString() != null
							|| !data.get("status").getAsString().isEmpty()), "status is null or empty");
				} else {
					sa.fail("data-status is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "data-status is missing..");
				}

				if (data.has("type")) {
					sa.assertTrue(!data.get("type").isJsonNull()
							&& (data.get("type").getAsString() != null || !data.get("type").getAsString().isEmpty()),
							"type is null or empty");
				} else {
					sa.fail("type is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "type is missing..");
				}

				if (data.has("created_at")) {
					sa.assertTrue(!data.get("created_at").isJsonNull() && (data.get("created_at").getAsString() != null
							|| !data.get("created_at").getAsString().isEmpty()), "created_at is null or empty");
				} else {
					sa.fail("created_at is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "created_at is missing..");
				}

				if (data.has("completed_at")) {
					sa.assertTrue(
							!data.get("completed_at").isJsonNull() && (data.get("completed_at").getAsString() != null
									|| !data.get("completed_at").getAsString().isEmpty()),
							"completed_at is null or empty");
				} else {
					sa.fail("completed_at is missing..");
					ExtentTestManager.getTest().log(LogStatus.WARNING, "completed_at is missing..");
				}

			} else {
				sa.fail("data is missing..");
				ExtentTestManager.getTest().log(LogStatus.WARNING, "data is missing..");
			}

			if (json.has("status")) {

				String status = json.get("status").getAsString();
				if (!status.equals("success")) {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "status is failure!");
				}

			} else {
//				sa.fail("data is missing..");
				ExtentTestManager.getTest().log(LogStatus.WARNING, "status is missing..");
			}

			if (json.has("totalResults")) {
				String totalResults = json.get("totalResults").getAsString();
				if (totalResults == null || totalResults.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "totalResults is null/empty");
				}

			} else {
//				sa.fail("totalResults is missing..");
				ExtentTestManager.getTest().log(LogStatus.WARNING, "totalResults is missing..");
			}

			if (json.has("code")) {
				String code = json.get("code").getAsString();
				sa.assertTrue(code.equals("200"), "Code is not equal to 200");
				if (code == null || code.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "code is null/empty");
				}

			} else {
//				sa.fail("totalResults is missing..");
				ExtentTestManager.getTest().log(LogStatus.WARNING, "code is missing..");
			}

			if (json.has("message")) {
				String message = json.get("message").getAsString();
//				sa.assertTrue(message.equals("200"), "Message is not equal to 200");
				if (message == null || message.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.WARNING, "message is null/empty");
				}

			} else {
//				sa.fail("totalResults is missing..");
				ExtentTestManager.getTest().log(LogStatus.WARNING, "message is missing..");
			}

		}
	}

}
