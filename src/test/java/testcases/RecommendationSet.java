/**
 * 
 */
package testcases;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import apis.GenerateToken;
import apis.RecoSet;
import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class RecommendationSet extends BaseTest {

	String recommendation_set_id = null;

	/*
	 * Create Recommendation Set
	 */
	@Test(priority = 0)
	@Parameters({ "server" })
	public void createRecommendationSet(String server) {

		ExtentTest createRS = ExtentTestManager.startTest("Create Recommendation Set",
				"Creates a new Recommendation Set");

		createRS.log(LogStatus.INFO, "Create Recos Set API");

		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = RecoSet.createRSPostReq(server);
			HttpResponse res = client.execute(post);
			System.out.println("REsponse......");
			String resp = EntityUtils.toString(res.getEntity());
			System.out.println(resp);
			createRS.log(LogStatus.INFO, "Response: " + resp);
			createRS.log(LogStatus.INFO, "Validating the response...");
			JsonParser parser = new JsonParser();

			JsonObject jsonResp = parser.parse(resp).getAsJsonObject();
			System.out.println("\n Printing the parsed json response....");
			System.out.println(jsonResp.toString());

			validateCreateRecoSet(jsonResp);

			createRS.log(LogStatus.INFO, "recommendation_set_id: " + this.recommendation_set_id);
			System.out.println("-------------------------------");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test(priority = 1)
	@Parameters({ "server" })
	public void getRecoSet(String server) {
		ExtentTest getRS = ExtentTestManager.startTest("Get Recommendation Set",
				"Gets the Recommendation Set with the given id");
		getRS.log(LogStatus.INFO, "Getting Reco Set details of " + this.recommendation_set_id);
		String url = server + "/recommendationset/get?env=qa&recommendation_set_id=" + this.recommendation_set_id
				+ "&access_token=" + GenerateToken.generateToken();
		HttpClient client = HttpClientBuilder.create().build();

		HttpGet get = new HttpGet(url);

		try {
			HttpResponse resp = client.execute(get);
			String response = EntityUtils.toString(resp.getEntity());
			System.out.println(response);
			getRS.log(LogStatus.INFO, response);

			JsonObject json = new JsonParser().parse(response).getAsJsonObject();
			validateGetRecoSet(json);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(priority = 3)
	@Parameters({ "server" })
	public void deleteRecoSet(String server) {
		ExtentTest delRS = ExtentTestManager.startTest("Delete Recommendation Set",
				"Deletes the Recommendation Set with the given id");
		delRS.log(LogStatus.INFO, "Deleting Reco Set details of " + this.recommendation_set_id);
		String url = server + "/recommendationset/delete?env=qa&recommendation_set_id=" + this.recommendation_set_id
				+ "&access_token=" + GenerateToken.generateToken();
		HttpClient client = HttpClientBuilder.create().build();

		HttpPut put = new HttpPut(url);

		try {
			HttpResponse resp = client.execute(put);
			String response = EntityUtils.toString(resp.getEntity());
			System.out.println(response);
			delRS.log(LogStatus.INFO, response);

			delRS.log(LogStatus.INFO, "Validating the response...");
			JsonParser parser = new JsonParser();

			JsonObject jsonResp = parser.parse(response).getAsJsonObject();

			JsonObject data = jsonResp.get("data").getAsJsonObject();

			this.recommendation_set_id = data.get("recommendation_set_id").getAsString();

			delRS.log(LogStatus.INFO, "recommendation_set_id: " + this.recommendation_set_id);

			validateCreateRecoSet(jsonResp);

			System.out.println("-------------------------------");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(priority = 2)
	@Parameters({ "server" })
	public void updateRecoSet(String server) {
		ExtentTest updateRS = ExtentTestManager.startTest("Updates Recommendation Set",
				"Updates a Recommendation Set with id");

		updateRS.log(LogStatus.INFO, "Updating the reco set " + this.recommendation_set_id);

		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = RecoSet.updateRSPostReq(server, this.recommendation_set_id);

			HttpResponse resp = client.execute(post);
			String response = EntityUtils.toString(resp.getEntity());
			updateRS.log(LogStatus.INFO, "update response: " + response);
			JsonObject json = new JsonParser().parse(response).getAsJsonObject();
			validateGetRecoSet(json);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	void validateCreateRecoSet(JsonObject json) {
		if (sa != null) {
			if (json.has("data")) {
				JsonObject data = json.get("data").getAsJsonObject();
				if (data.has("recommendation_set_id")) {
					String rsid = data.get("recommendation_set_id").getAsString();
					sa.assertTrue(rsid != null && !rsid.isEmpty());
					this.recommendation_set_id = rsid;
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "recommendation_set_id is missing");
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "data is missing");
			}

			if (json.has("status")) {
				String status = json.get("status").getAsString();
				sa.assertTrue(status != null && !status.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "status is missing");
			}

			if (json.has("totalResults")) {
				String totalResults = json.get("totalResults").getAsString();
				sa.assertTrue(totalResults != null && !totalResults.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "totalResults is missing");
			}

			if (json.has("code")) {
				String code = json.get("code").getAsString();
				sa.assertTrue(code != null && !code.isEmpty() && code.equals("200"));
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "code is missing");
			}

			if (json.has("message")) {
				String message = json.get("message").getAsString();
				sa.assertTrue(message != null && !message.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "message is missing");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.ERROR, "SoftAssert instance is null");
		}
	}

	void validateGetRecoSet(JsonObject json) {
		if (sa != null) {
			if (json.has("data")) {
				JsonObject data = json.get("data").getAsJsonObject();
				if (data.has("recommendation_set_id")) {
					String rsid = data.get("recommendation_set_id").getAsString();
					sa.assertTrue(rsid != null && !rsid.isEmpty());
					this.recommendation_set_id = rsid;
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "recommendation_set_id is missing");
				}

				if (data.has("campaign_id")) {
					String campid = data.get("campaign_id").getAsString();
					sa.assertTrue(campid != null && !campid.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "campaign_id is missing");
				}

				if (data.has("recommendation_set_name")) {
					String recommendation_set_name = data.get("recommendation_set_name").getAsString();
					sa.assertTrue(recommendation_set_name != null && !recommendation_set_name.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "recommendation_set_name is missing");
				}

				if (data.has("start_date")) {
					String start_date = data.get("start_date").getAsString();
					sa.assertTrue(start_date != null && !start_date.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "start_date is missing");
				}

				if (data.has("end_date")) {
					String end_date = data.get("end_date").getAsString();
					sa.assertTrue(end_date != null && !end_date.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "end_date is missing");
				}

				if (data.has("daily_budget")) {
					if (data.get("daily_budget").isJsonNull()) {
						ExtentTestManager.getTest().log(LogStatus.WARNING, "daily_budget is null");
					} else {
						String daily_budget = data.get("daily_budget").getAsString();
//						System.out.println("daily_budget: " + data.get("daily_budget").isJsonNull());
						sa.assertTrue(daily_budget != null && !daily_budget.isEmpty());
					}

				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "daily_budget is missing");
				}

				if (data.has("advertiser")) {
					String advertiser = data.get("advertiser").getAsString();
					sa.assertTrue(advertiser != null && !advertiser.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "advertiser is missing");
				}

				if (data.has("cost_per_unit")) {
					String cost_per_unit = data.get("cost_per_unit").getAsString();
					sa.assertTrue(cost_per_unit != null && !cost_per_unit.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "cost_per_unit is missing");
				}

				if (data.has("geo_country")) {
					JsonArray geo_country = data.get("geo_country").getAsJsonArray();
					sa.assertTrue(!geo_country.isJsonNull() && geo_country.size() >= 0);
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "geo_country is missing");
				}

				if (data.has("external_recommendation_set_id")) {
					String external_recommendation_set_id = data.get("external_recommendation_set_id").getAsString();
					sa.assertTrue(external_recommendation_set_id != null && !external_recommendation_set_id.isEmpty());
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "external_recommendation_set_id is missing");
				}

				sa.assertTrue(data.has("comments"));

				if (data.has("platform")) {
					JsonArray platform = data.get("platform").getAsJsonArray();
					sa.assertTrue(!platform.isJsonNull() && platform.size() >= 0);
				} else {
					ExtentTestManager.getTest().log(LogStatus.ERROR, "platform is missing");
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "data is missing");
			}

			if (json.has("status")) {
				String status = json.get("status").getAsString();
				sa.assertTrue(status != null && !status.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "status is missing");
			}

			if (json.has("totalResults")) {
				String totalResults = json.get("totalResults").getAsString();
				sa.assertTrue(totalResults != null && !totalResults.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "totalResults is missing");
			}

			if (json.has("code")) {
				String code = json.get("code").getAsString();
				sa.assertTrue(code != null && !code.isEmpty() && code.equals("200"));
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "code is missing");
			}

			if (json.has("message")) {
				String message = json.get("message").getAsString();
				sa.assertTrue(message != null && !message.isEmpty());
			} else {
				ExtentTestManager.getTest().log(LogStatus.ERROR, "message is missing");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.ERROR, "SoftAssert instance is null");
		}
	}

}
