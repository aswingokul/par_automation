/**
 * 
 */
package testcases;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import apis.ExtendedSoftAssert;
import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class BaseTest {
	
	protected static SoftAssert sa = null;
	
	@BeforeSuite (description = "Initializes the reporting and assert libs", alwaysRun = true)
	public void setup() {
		
		ExtentTest setupTest = ExtentTestManager.startTest("Setup", "Initializes the reporting and assert libs");
		
		System.out.println("Setup....");
		
		sa = getSoftAssert();
		
		setupTest.log(LogStatus.INFO, "Extent Report is initialized");
		setupTest.log(LogStatus.INFO, "Assert is initialized");
		
		
	}
	
	@AfterSuite (description = "Quits all the opened resources", alwaysRun = true)
	public void tearDown() {
		ExtentTest teardown = ExtentTestManager.startTest("Teardown", "Outputs all the assertion failures");
		System.out.println("teardown....");
		sa.assertAll();
		teardown.log(LogStatus.INFO, "All assertion failures are flushed");
	}
	
	public static SoftAssert getSoftAssert() {
		if(sa == null) {
			sa = new ExtendedSoftAssert();
		}
		return sa;
	}

}
