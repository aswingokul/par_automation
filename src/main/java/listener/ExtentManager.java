/**
 * 
 */
package listener;

import com.relevantcodes.extentreports.ExtentReports;

/**
 * @author aswingokulachandran
 *
 */
public class ExtentManager {
	
	private static ExtentReports extent= null;
	
	public static ExtentReports getReporter() {
		if(extent == null) {
			
			extent = new ExtentReports("reports/ExtentReportsResults.html", true);
		}
		return extent;
	}

}
