/**
 * 
 */
package listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.LogStatus;

/**
 * @author aswingokulachandran
 *
 */
public class TestListener implements ITestListener {
	
	public static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("I'm onTestStart Method: " + getTestMethodName(result) + " start");
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		ExtentTestManager.getTest().log(LogStatus.INFO, getTestMethodName(result) + " succeeded");		
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
//		System.out.println("I'm onTestSkipped Method: " + getTestMethodName(result) + " skipped");
		ExtentTestManager.getTest().log(LogStatus.SKIP, getTestMethodName(result) + " skipped");
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("I'm onTestFailedButWithinSuccessPercentage Method: " + getTestMethodName(result) + " failed but within success ratio");
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
//		System.out.println("I'm onStart Method: " + context.getName());
		ExtentTestManager.getTest().log(LogStatus.INFO, "Tests for " + context.getName() + " has started...");
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		ExtentTestManager.getTest().log(LogStatus.INFO, "Tests for " + context.getName() + " has ended...");
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		
		
	}

}
