/**
 * 
 */
package apis;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.relevantcodes.extentreports.LogStatus;

import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class Recos {
	
	public static HttpPost createRecommendation(String server, String recommendation_set_id) throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Building the HTTP POST request...");
		StringBuilder url = new StringBuilder(server + "/recommendation/create");
		URIBuilder builder = new URIBuilder(url.toString());

		ArrayList<NameValuePair> bodyParams = new ArrayList<>();

		bodyParams.add(new BasicNameValuePair("recommendation_set_id", recommendation_set_id));
		bodyParams.add(new BasicNameValuePair("start_date", "2020-03-20T00:00:00-05:30"));
		bodyParams.add(new BasicNameValuePair("bid_multiplier", "2"));
		bodyParams.add(new BasicNameValuePair("ad_ids", "[\"adavick7\",\"adavick6\"]"));
		bodyParams.add(new BasicNameValuePair("creative_id", "[\"5cef0b30d1af0\",\"5cf0c56857f5b\"]"));
		bodyParams.add(new BasicNameValuePair("access_token", GenerateToken.generateToken()));
		bodyParams.add(new BasicNameValuePair("env", "qa"));
		System.out.println("bodyParams: " + bodyParams.toString());
		HttpPost post = new HttpPost(builder.build());
		post.setEntity(new UrlEncodedFormEntity(bodyParams));

		return post;
	}
	
	

}
