/**
 * 
 */
package apis;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;

import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author Tanu
 *
 */
public class GenerateToken {

	// static member holds only one instance of the GenerateToken class.
	private static GenerateToken accessToken;

	// GenerateToken prevents the instantiation from any other class.
	private GenerateToken() {

	}

	// Now we are providing glabal point of access.
	public static GenerateToken getInstance() {
		if (accessToken == null) {
			accessToken = new GenerateToken();
		}
		return accessToken;

	}

	// to get the Access Token from method

	public static String generateToken() {

		String token = null;

		try {
			String url = "http://amazon-ads-qa-1968130785.ap-southeast-1.elb.amazonaws.com/api/v1/generateToken/create?env=qa";
			HttpClient client = HttpClientBuilder.create().build();

			HttpPost post = new HttpPost(url);
			JsonObject reqJson = new JsonObject();
			reqJson.addProperty("grant_type", "client_credentials");
			reqJson.addProperty("client_id", "SaavnAPITestCLient");
			reqJson.addProperty("client_secret", "SaavnAPITestPass");
			StringEntity strEntity = new StringEntity(reqJson.toString());
			post.setEntity(strEntity);
			post.setHeader("Content-Type", "application/json");
			HttpResponse resp = client.execute(post);
			HttpEntity respEntity = resp.getEntity();
			String respStr = EntityUtils.toString(respEntity);
			JsonObject respJsonObj = new JsonParser().parse(respStr).getAsJsonObject();
			System.out.println("Json is " + respJsonObj);
			token = respJsonObj.get("access_token").getAsString();
			System.out.println("access_token: " + token);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;

	}

	public static String generateToken(String clientId, String clientPass) {

		String token = null;

		try {
			String url = "http://amazon-ads-qa-1968130785.ap-southeast-1.elb.amazonaws.com/api/v1/generateToken/create?env=qa";
			HttpClient client = HttpClientBuilder.create().build();

			HttpPost post = new HttpPost(url);
			JsonObject reqJson = new JsonObject();
			reqJson.addProperty("grant_type", "client_credentials");
			reqJson.addProperty("client_id", clientId);
			reqJson.addProperty("client_secret", clientPass);
			StringEntity strEntity = new StringEntity(reqJson.toString());
			post.setEntity(strEntity);
			post.setHeader("Content-Type", "application/json");
			HttpResponse resp = client.execute(post);
			HttpEntity respEntity = resp.getEntity();
			String respStr = EntityUtils.toString(respEntity);
			JsonObject respJsonObj = new JsonParser().parse(respStr).getAsJsonObject();
			System.out.println("json" + respJsonObj);
			token = respJsonObj.get("access_token").getAsString();
			System.out.println("access_token: " + token);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;

	}

}
