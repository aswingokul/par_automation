/**
 * 
 */
package apis;

import org.testng.asserts.IAssert;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class ExtendedSoftAssert extends SoftAssert {
	
	@Override
	public void onAssertFailure(IAssert<?> assertCommand, AssertionError error) {
		String message = String.format("Expected [%s] but found [%s]", assertCommand.getExpected().toString(), assertCommand.getActual().toString());
//		ExtentTestManager.getTest().log(LogStatus.ERROR, message);
		ExtentTest test = ExtentTestManager.getTest();
		if(test == null) {
			System.err.println("ExtendedSoftAssert: test is null...");
		}else {
			test.log(LogStatus.ERROR, assertCommand.getMessage().toString() + " <FAILED>. " + message);
		}
		
	}

}
