/**
 * 
 */
package apis;

import java.io.FileReader;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.relevantcodes.extentreports.LogStatus;

import listener.ExtentTestManager;

/**
 * @author aswingokulachandran
 *
 */
public class RecoSet {

	public static HttpPost createRSPostReq(String server) throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Building the HTTP POST request...");
		StringBuilder url = new StringBuilder(server + "/recommendationset/create");
		URIBuilder builder = new URIBuilder(url.toString());

		ArrayList<NameValuePair> bodyParams = new ArrayList<>();

		bodyParams.add(new BasicNameValuePair("campaign_id", "5cf84c065db79"));
		bodyParams.add(new BasicNameValuePair("start_date", "2020-03-20T00:00:00-05:30"));
		bodyParams.add(new BasicNameValuePair("end_date", "2020-03-21T00:00:00-05:30"));
		bodyParams.add(new BasicNameValuePair("recommendation_set_name", "bodytest_2"));
		bodyParams.add(new BasicNameValuePair("daily_budget", "50"));
		bodyParams.add(new BasicNameValuePair("cost_per_unit", "1.2"));
		bodyParams.add(new BasicNameValuePair("geo_country", "[\"US\", \"IN\"]"));
		bodyParams.add(new BasicNameValuePair("access_token", GenerateToken.generateToken()));
		bodyParams.add(new BasicNameValuePair("env", "qa"));
		System.out.println("bodyParams: " + bodyParams.toString());
		HttpPost post = new HttpPost(builder.build());
		post.setEntity(new UrlEncodedFormEntity(bodyParams));

		return post;
	}

	public static HttpPost updateRSPostReq(String server, String recommendation_set_id) throws Exception {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Building the HTTP POST request...");
		String url = server + "/recommendationset/update";
		URIBuilder builder = new URIBuilder(url);
		
		ArrayList<NameValuePair> params = new ArrayList<>();
		
		String filePath = "src/main/java/properties/RecoSetUpdate.json";
		
		JsonObject obj = new Gson().fromJson(new JsonReader(new FileReader(filePath)), JsonObject.class);
		
		params.add(new BasicNameValuePair("start_date", obj.get("start_date").getAsString()));
		params.add(new BasicNameValuePair("end_date", obj.get("end_date").getAsString()));
		params.add(new BasicNameValuePair("env", obj.get("env").getAsString()));
		params.add(new BasicNameValuePair("access_token", GenerateToken.generateToken()));
		params.add(new BasicNameValuePair("recommendation_set_id", recommendation_set_id));
		
		HttpPost post = new HttpPost(builder.build());
		post.setEntity(new UrlEncodedFormEntity(params));
		
		return post;
		
	}

}
